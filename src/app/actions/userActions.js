import { setToken } from 'src/utils/cookies';

export const userLoginAction = (dataForm) => async (dispatch) => {
  const typeAction = await import('src/app/type-actions');
  const apiUser = await import('src/api/user');
  try {
    dispatch({ type: typeAction.USER_LOGIN_BEGIN });

    const dataLogin = await apiUser.userLogin(dataForm);

    dispatch({
      type: typeAction.USER_LOGIN_SUCCESS,
      payload: dataLogin,
    });

    const dataUser = await apiUser.getUser();

    dispatch({
      type: typeAction.USER_ACCOUNT_SUCCESS,
      payload: dataUser,
    });
  } catch (error) {
    dispatch({
      type: typeAction.USER_LOGIN_FAIL,
      payload: { ...error.response.data },
    });
  }
};

export const getUserAction = () => async (dispatch) => {
  const apiUser = await import('src/api/user');
  const typeAction = await import('src/app/type-actions');

  try {
    const data = await apiUser.getUser();

    dispatch({
      type: typeAction.USER_ACCOUNT_SUCCESS,
      payload: { ...data },
    });
  } catch (error) {
    dispatch({
      type: typeAction.USER_ACCOUNT_ERROR,
      payload: { message: error.response.data.message, user: undefined },
    });
  }
};

export const userRegisterAction = (user) => async (dispatch) => {
  const apiUser = await import('src/api/user');
  const typeAction = await import('src/app/type-actions');
  try {
    dispatch({ type: typeAction.USER_REGISTER_BEGIN });

    const { data, token, message } = await apiUser.registerUser(user);

    dispatch({
      type: typeAction.USER_REGISTER_SUCCESS,
      payload: { data, token, message },
    });
  } catch (error) {
    dispatch({
      type: typeAction.USER_REGISTER_FAIL,
      payload: error.response.data.errors || error.message,
    });
  }
};

// Modify state of :user
/**
 *
 * @param {*} params
 * @returns
 */
export const userUpdateAction = (params) => async (dispatch) => {
  const typeAction = await import('src/app/type-actions');

  try {
    dispatch({ type: typeAction.USER_UPDATE_BEGIN });

    dispatch({
      type: typeAction.USER_UPDATE_SUCCESS,
      payload: { user: params.data, message: params.message },
    });
  } catch (error) {
    dispatch({
      type: typeAction.USER_UPDATE_FAIL,
      payload: error.response ? error.response.data.message : error.message,
    });
  }
};

export const userConfirmAction = (token) => async (dispatch) => {
  const apiUser = await import('src/api/user');
  const typeAction = await import('src/app/type-actions');
  try {
    dispatch({ type: typeAction.AUTH_CONFIRM_BEGIN });

    const { message, data } = await apiUser.userConfirmAccount(token);

    dispatch({
      type: typeAction.AUTH_CONFIRM_SUCCESS,
      payload: message,
    });
    dispatch({ type: typeAction.USER_LOGIN_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: typeAction.AUTH_CONFIRM_FAIL,
      payload: error.response ? error.response.data.message : error.message,
    });
  }
};

export const userLogoutAction = () => async (dispatch) => {
  const apiUser = await import('src/api/user');
  const typeAction = await import('src/app/type-actions');
  dispatch({ type: typeAction.USER_LOGOUT_BEGIN });
  await apiUser.userLogout();
  setToken('');
  dispatch({ type: typeAction.USER_LOGOUT_SUCCESS });
  dispatch({ type: typeAction.RESET_ERROR_SUCCESS_MESSAGE });
};

export const removeImageAccountAction = () => async (dispatch) => {
  const apiUser = await import('src/api/user');
  const typeAction = await import('src/app/type-actions');
  try {
    dispatch({ type: typeAction.USER_UPDATE_BEGIN });
    const res = await apiUser.removeImageUser();
    dispatch({
      type: typeAction.USER_UPDATE_SUCCESS,
      payload: { ...res },
    });
  } catch (error) {
    dispatch({
      type: typeAction.USER_UPDATE_FAIL,
      payload: error.response ? error.response.data.message : error.message,
    });
  }
};
