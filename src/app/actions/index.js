import * as typeAction from 'src/app/type-actions/userTypeActions';
export * from './userActions';

export const resetAction = () => (dispatch) => {
  dispatch({ type: typeAction.RESET_ERROR_SUCCESS_MESSAGE });
};
