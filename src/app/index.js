import { createStore, compose, applyMiddleware } from 'redux';
import middlewareThunk from 'redux-thunk';
import { appReducers } from './reducers';
import { ENV_DEV } from 'src/configApp';
import { getTokenAuth, setToken } from 'src/utils/cookies';

let initialState = {};

const middlewares = [middlewareThunk];

const composeEnhancers =
  (ENV_DEV && window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']) || compose || compose;

const store = createStore(
  appReducers,
  initialState,
  composeEnhancers(applyMiddleware(...middlewares))
);

store.subscribe(() => {
  let token = store.getState().authenticate.token || getTokenAuth();
  setToken(token);
});

export default store;
