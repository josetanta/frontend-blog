import { combineReducers } from 'redux';
import * as userReducers from './userReducers';

export const appReducers = combineReducers({
  authenticate: userReducers.userLoginReducer,
});
