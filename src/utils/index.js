/**
 *
 * @param {*} obj
 * @returns {any}
 */
export function parseArrAndObj(obj) {
  let reObj = obj;
  if (reObj instanceof Array) {
    let res = reObj.reduce((prevInit, current) => {
      let msg = current.message.reduce((p, c) => p + ' ' + c, ' ');
      let field = current.field;
      return prevInit.concat(`${field}: ${msg}`);
    }, []);
    return res;
  } else if (reObj instanceof String) {
    return reObj;
  } else {
    return reObj;
  }
}
