export function isFollowing(followedId, follows = []) {
  return follows.find((f) => f.id === followedId);
}
