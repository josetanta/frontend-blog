import { createCipheriv, randomBytes } from 'crypto';

const ALGORITHM = 'aes-256-ccm';
const SECRET_KEY = process.env.REACT_APP_SECRET_KEY || 'secret-key';
const iv = randomBytes(64);

export function encrypt(data) {
  const cipher = createCipheriv(ALGORITHM, SECRET_KEY, iv);

  const encrypted = Buffer.concat([cipher.update(data), cipher.final()]);

  return {
    iv: iv.toString('base64'),
    content: encrypted.toString('base64'),
  };
}
