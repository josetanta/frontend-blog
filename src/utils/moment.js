import moment from 'moment';
import 'moment/locale/es';

export function formatDatetime(dt) {
  let temp = moment(new Date(dt)).locale('es');
  return temp.startOf('hour').fromNow();
}
