import React from 'react';
// Components
import { AuthPermFollow } from 'src/components/utils/AuthPerm';
import { Button, Card, Col, OverlayTrigger, Popover, Row, Spinner } from 'react-bootstrap';
import PostList from 'src/components/PostList';

import { useUserDetail } from 'src/hooks/authentication';
import FollowList from 'src/components/follow/FollowList';
import FollowButton from 'src/components/follow/FollowButton';
import { useFollowedList, useFollowersList } from 'src/hooks';

function UserPage(props) {
  const { uuidUser } = props.match.params;

  const userDetail = useUserDetail(uuidUser);
  const followersList = useFollowersList(uuidUser);
  const followedList = useFollowedList(uuidUser);
  let attr = userDetail.data?.user.attributes;

  const followersNode = (
    <Popover id='popover-basic-followers' placement='bottom'>
      <Popover.Title as='h3'>
        Seguidores de <strong>{attr?.email}</strong>
      </Popover.Title>
      <Popover.Content>
        {followersList.isLoading ? (
          <div>Cargando datos</div>
        ) : followersList.isError ? (
          <div>Error al cargar</div>
        ) : (
          <FollowList follows={followersList.data.followers} />
        )}
      </Popover.Content>
    </Popover>
  );
  const followedNode = (
    <Popover id='popover-basic-followed' placement='top'>
      <Popover.Title as='h3'>
        <strong>{attr?.email}</strong> esta siguiendo
      </Popover.Title>
      <Popover.Content>
        {followedList.isLoading ? (
          <div>Cargando datos</div>
        ) : followedList.isError ? (
          <div>Error al cargar</div>
        ) : (
          <FollowList follows={followedList.data.followed} />
        )}
      </Popover.Content>
    </Popover>
  );

  return (
    <React.Fragment>
      <Row>
        <Col
          className='d-flex flex-column flex-md-row align-items-center justify-content-center gap-3'
          xl='12'
          lg='12'
          sm='12'
          md='12'
        >
          {userDetail.isLoading ? (
            <Spinner animation='border' variant='primary' />
          ) : (
            <React.Fragment>
              <Card className='border-0 w-full'>
                <Card.Body className='relative d-flex flex-nowrap flex-column border-0 w-full justify-content-center'>
                  <Card.Img
                    src={attr.image}
                    className='user__page__img rounded'
                    alt={attr.username}
                  />
                  <AuthPermFollow user={userDetail.data.user}>
                    <div className='absolute follow__direction my-3 d-flex flex-nowrap justify-content-center'>
                      <FollowButton
                        follows={followersList.data?.followers}
                        email={attr.email}
                        uuidUser={uuidUser}
                        userId={userDetail.data.user?.id}
                      />
                    </div>
                  </AuthPermFollow>
                </Card.Body>
              </Card>
              <Card className='d-flex w-full'>
                <Card.Body className='d-flex flex-column flex-nowrap gap-3  align-content-center justify-content-center'>
                  <Card.Title className='text-center text-success'>
                    <h4>{attr.username}</h4>
                    <h6>{attr.email}</h6>
                    <p>{attr.name}</p>
                  </Card.Title>

                  <div className='d-flex w-full flex-column flex-wrap gap-4 justify-content-between align-items-center'>
                    <OverlayTrigger trigger='click' placement='top' overlay={followedNode}>
                      <Button
                        variant='outline-primary'
                        className='d-flex w-auto flex-nowrap secondary gap-3 justify-content-between align-items-center'
                      >
                        <span>Siguiendo</span>
                        <span className='badge badge-primary'>{followedList.data.count}</span>
                      </Button>
                    </OverlayTrigger>

                    <OverlayTrigger trigger='click' placement='bottom' overlay={followersNode}>
                      <Button
                        variant='outline-primary'
                        className='d-flex gap-3 w-auto flex-nowrap justify-content-between align-items-center'
                      >
                        <span>Seguidores</span>
                        <span className='badge badge-primary'>{followersList.data?.count}</span>
                      </Button>
                    </OverlayTrigger>
                  </div>
                </Card.Body>
              </Card>
            </React.Fragment>
          )}
        </Col>
      </Row>
      <Row className='justify-content-center items-align-center'>
        {userDetail.isLoading ? (
          <Spinner animation='border' variant='primary' />
        ) : (
          <PostList col={9} posts={userDetail.data.posts} />
        )}
      </Row>
    </React.Fragment>
  );
}

export default UserPage;
