import { Button, Container, Jumbotron } from 'react-bootstrap';
export default function Error404Page(props) {
  return (
    <Jumbotron className='bg-danger text-light'>
      <Container>
        <h1>Ups Parece no encuetras lo que buscas</h1>
        <Button variant='light' onClick={props.history.goBack}>
          Regresar
        </Button>
      </Container>
    </Jumbotron>
  );
}
