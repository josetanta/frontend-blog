import React from 'react';
import { useDispatch } from 'react-redux';
import { Link, Redirect, useHistory } from 'react-router-dom';

import { Col, Container, Jumbotron, Row, Spinner } from 'react-bootstrap';
import { useUserAuth } from 'src/hooks/authentication';
import { userConfirmAction, getUserAction } from 'src/app/actions/userActions';

function ConfirmAccountPage(props) {
  const userAuth = useUserAuth();

  const history = useHistory();
  const dispatch = useDispatch();

  React.useEffect(() => {
    let search = props.location.search;
    let queries = new URLSearchParams(search);
    if (queries.has('token')) {
      let token = queries.get('token');
      dispatch(userConfirmAction(token));
      dispatch(getUserAction());
    }
    let timeout = setTimeout(() => {
      dispatch(getUserAction());
      userAuth.success && history.push('/profile?is_confirmed');
    }, 3000);

    return () => {
      clearTimeout(timeout);
    };
  }, [props.location.search, dispatch, userAuth.success, history]);

  if (!userAuth.isAuth) {
    return <Redirect to={props.history.goBack() || '/profile'} exact push />;
  }

  if (userAuth.isAuth && userAuth.user?.confirmed) {
    return <Redirect to={props.history.goBack() || '/profile'} exact push />;
  }

  return (
    <Row>
      <Col sm={12} xl={12} md={12}>
        <Container>
          <Jumbotron fluid className='p-4'>
            {!!userAuth.message && <div className='alert alert-success'>{userAuth.message}</div>}
            <h3>
              <Spinner animation='grow' variant='primary' /> <br /> Un momento por favor estamos
              autenticando su cuenta 😄.
            </h3>
            {userAuth.success && <h3 className='text-primary'>Se Autentico su cuenta.</h3>}
            {userAuth.error ? (
              <React.Fragment>
                <h3 className='text-danger'>{userAuth.error}</h3>
                <div>
                  Puede reenviar el mensage de confirmacion{' '}
                  <Link className='btn btn-outline-secondary' to='/unconfirmed-account'>
                    Aqui
                  </Link>
                </div>
              </React.Fragment>
            ) : (
              userAuth.user?.confirmed &&
              userAuth.success && <h2 className='text-success'>Usted a confirmado su cuenta</h2>
            )}
          </Jumbotron>
        </Container>
      </Col>
    </Row>
  );
}

export default ConfirmAccountPage;
