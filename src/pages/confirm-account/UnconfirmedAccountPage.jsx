import { useState } from 'react';
import { Button, Col, Container, Jumbotron, Row } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { reseendTokenConfirm } from 'src/api/user';
import { useUserAuth } from 'src/hooks/authentication';

function UnconfirmedAccountPage(props) {
  const userAuth = useUserAuth();
  const [message, setMessage] = useState('');

  const handleResendTokenClick = async () => {
    let { message } = await reseendTokenConfirm();
    setMessage(message);
  };

  if (!userAuth.isAuth) return <Redirect to='/posts' exact push />;

  if (userAuth.isAuth && userAuth.user?.confirmed) {
    return <Redirect to={props.history.goBack() || '/'} exact push />;
  }
  return (
    <Row>
      <Col sm={12} xl={12} md={12}>
        <Jumbotron fluid>
          <Container>
            {message && <div className='alert alert-success'>{message}</div>}
            <h1>Tiene que confirmar su cuenta por favor!</h1>
            <div>
              Si aun no confirmado su cuenta por favor haga click{'  '}
              <Button as='a' variant='dark	' onClick={handleResendTokenClick}>
                Aqui
              </Button>
              {'  '}
              para poder reenviarle el mensage de confirmación.
            </div>
          </Container>
        </Jumbotron>
      </Col>
    </Row>
  );
}

export default UnconfirmedAccountPage;
