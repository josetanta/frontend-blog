import React from 'react';
import { useQueryClient } from 'react-query';
// Components
import { Alert, Button, Col, Jumbotron, Row, Spinner } from 'react-bootstrap';
import Post from 'src/components/Post';
import CommentForm from 'src/components/forms/CommentForm';
import PostForm from 'src/components/forms/PostForm';
import CommentList from 'src/components/CommentList';
import PostHeader from 'src/components/PostHeader';
import withErrorBoundary from 'src/hocs/withErrorBoundary';

// Hooks
import { AuthPermWrite } from 'src/components/utils/AuthPerm';
import { useCommentsListByPost, usePostDetail } from 'src/hooks';
import { parseArrAndObj } from 'src/utils';

function PostPage(props) {
  let { uuidPost } = props.match.params;
  const [editPost, setEditPost] = React.useState(false);
  const [isComment, setIsComment] = React.useState(false);
  const [showAlert, setShowAlert] = React.useState(false);
  const postDetail = usePostDetail(uuidPost);
  const commentList = useCommentsListByPost(uuidPost);
  const queryClient = useQueryClient();

  let errorsComment = queryClient.getQueryData(['errors', uuidPost], { stale: false });

  const handleCloseClick = () => {
    queryClient.setQueryData(['errors', uuidPost], undefined);
    setShowAlert(!showAlert);
  };

  const handleEditClick = () => {
    setEditPost(!editPost);
  };

  const commentListNode = (
    <Col sm='12' md='6' lg='6' xl='4'>
      <AuthPermWrite message='Debe de confirmar su cuenta para que pueda comentar esta publicación'>
        {(isComment && (
          <React.Fragment>
            <CommentForm
              uuidPost={uuidPost}
              postId={postDetail.data.id}
              setIsComment={setIsComment}
              isComment={isComment}
            />
          </React.Fragment>
        )) || (
          <React.Fragment>
            <Button
              className='d-flex gap-3 justify-content-around align-items-center'
              onClick={() => setIsComment(!isComment)}
            >
              <i className='fal fa-comments' />
              <span>Crear un comentario</span>
            </Button>
            {errorsComment?.length > 0 && (
              <Alert dismissible onClose={handleCloseClick} className='my-2' variant='danger'>
                <span>{parseArrAndObj(errorsComment)}</span>
              </Alert>
            )}
          </React.Fragment>
        )}
      </AuthPermWrite>

      {commentList.isLoading ? (
        <Spinner animation='border' variant='secondary' />
      ) : commentList.isError ? (
        <div>Error</div>
      ) : (
        <CommentList
          isLoading={postDetail.isLoading}
          isError={postDetail.isError}
          comments={commentList.data}
        />
      )}
    </Col>
  );

  if (editPost) {
    return (
      <Row>
        <Col md={12} sm={12} xl={12} lg={12}>
          <div>
            <Button
              variant='dark'
              className='w-auto gap-3 d-flex justify-content-between align-items-center'
              onClick={handleEditClick}
            >
              <i className='far fa-times' />
              <span>Cancelar</span>
            </Button>
          </div>
          <PostForm post={postDetail.data} setEditPost={setEditPost} editPost={editPost} />
        </Col>
      </Row>
    );
  }

  return (
    <Jumbotron>
      <Row className='align-items-center'>
        <Col sm='12'>
          <Jumbotron fluid className='text-center'>
            <PostHeader post={postDetail.data} />
          </Jumbotron>
        </Col>
      </Row>
      <Row className='d-flex'>
        <Col sm='12' md='6' lg='6' xl='8'>
          {postDetail.isLoading ? (
            <Spinner animation='border' variant='primary' />
          ) : postDetail.isError ? (
            <div>Error</div>
          ) : (
            <Post post={postDetail.data} showActions onEditPost={setEditPost} />
          )}
        </Col>
        {commentListNode}
      </Row>
    </Jumbotron>
  );
}

export default withErrorBoundary(PostPage);
