import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

import ProtectedRoute from './ProtectedRoute';

import RegisterPage from 'src/pages/RegisterPage';
import AboutPage from 'src/pages/AboutPage';
import HomePage from 'src/pages/HomePage';
import Error404Page from 'src/pages/handlers/Error404Page';
import AuthAccountProtected from './AuthAccountProtected';
import ConfirmAccountPage from 'src/pages/confirm-account/ConfirmAccountPage';
import UnconfirmedAccountPage from 'src/pages/confirm-account/UnconfirmedAccountPage';
import { Container, Spinner } from 'react-bootstrap';
const PostListPageLazy = React.lazy(() => import('src/pages/PostListPage'));
const PostPageLazy = React.lazy(() => import('src/pages/PostPage'));
const UserPageLazy = React.lazy(() => import('src/pages/UserPage'));
const AccountPageLazy = React.lazy(() => import('src/pages/AccountPage'));

const fallback = <Spinner className='spinner__lazy' animation='border' variant='success' />;

function AppRoute() {
  const location = useLocation();

  return (
    <React.Suspense fallback={fallback}>
      <Container>
        <Switch location={location}>
          <ProtectedRoute path='/confirm-account' component={ConfirmAccountPage} />

          <Route location={location} path='/posts/:uuidPost' component={PostPageLazy} />

          <Route path='/user/:uuidUser' component={UserPageLazy} />

          <ProtectedRoute exact path='/unconfirmed-account' component={UnconfirmedAccountPage} />

          <AuthAccountProtected exact path='/profile' component={AccountPageLazy} />
          <Route path='/register' component={RegisterPage} />

          <Route path='/about' component={AboutPage} />
          <Route exact path='/posts' component={PostListPageLazy} />
          <Route exact path={['/', '/inicio']} component={HomePage} />

          <Route path='*' component={Error404Page} />
        </Switch>
      </Container>
    </React.Suspense>
  );
}

export default AppRoute;
