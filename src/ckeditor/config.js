export const editorConfiguration = {
  toolbar: {
    items: [
      'heading',
      'bold',
      'italic',
      'link',
      'bulletedList',
      'numberedList',
      'undo',
      'redo',
      'blockQuote',
      'insert'
    ],
  },
};
