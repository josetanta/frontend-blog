import AppRoute from 'src/routes/AppRoute';

// Components
import GeneralProvider from 'src/providers/GeneralProvider';
import Navigation from 'src/components/Navigation';
import Footer from 'src/components/Footer';
import AppRoot from 'src/components/AppRoot';

function BlogApp() {
  return (
    <GeneralProvider>
      <Navigation />
      <AppRoot>
        <AppRoute />
      </AppRoot>
      <Footer />
    </GeneralProvider>
  );
}

export default BlogApp;
