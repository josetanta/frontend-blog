import { clientTokenRequest, clientAnonymousRequest } from './axiosClient';

async function getPost(uuid) {
  const { client } = clientAnonymousRequest();
  const { data } = (await client.get(`/posts/${uuid}/`)).data;

  return { data };
}

async function getPostList(page) {
  const { client } = clientAnonymousRequest();
  const { data, count, pages } = (await client.get(`/posts/?page=${page}`)).data;

  return {
    data,
    count,
    pages,
  };
}

async function savePost(post) {
  const { client } = clientTokenRequest();
  const { data } = (await client.post(`/posts/`, post)).data;
  return {
    data,
  };
}

async function deletePost(postId) {
  const { client } = clientTokenRequest();
  let { res } = (await client.delete(`/posts/${postId}/`)).data;
  return {
    res,
  };
}

async function updatePost(postId, post) {
  const { client } = clientTokenRequest();

  await client.put(`/posts/${postId}/`, post);

  const { data, message } = (await getPost(postId)).data;

  return { data, message };
}

export { getPost, savePost, deletePost, getPostList, updatePost };
