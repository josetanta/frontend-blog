import { clientAnonymousRequest, clientTokenRequest } from './axiosClient';

async function getFollowers(uuid) {
  const { client } = clientAnonymousRequest();

  let { data, count } = (await client.get(`/followers/${uuid}/`)).data;

  return { followers: data, count };
}

async function getFollowed(uuid) {
  const { client } = clientAnonymousRequest();

  let { data, count } = (await client.get(`/followed/${uuid}/`)).data;

  return { followed: data, count };
}

async function follow(userId) {
  const { client } = clientTokenRequest();

  let { message } = (await client.post(`/users/${userId}/follow/`)).data;

  return { message };
}

async function unfollow(userId) {
  const { client } = clientTokenRequest();

  let { message } = (await client.post(`/users/${userId}/unfollow/`)).data;
  return { message };
}

export { follow, unfollow, getFollowers, getFollowed };
