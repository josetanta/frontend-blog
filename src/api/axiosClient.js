import axios from 'axios';
import store from 'src/app';
import { BASE_URL } from 'src/configApp';
import { getTokenAuth } from 'src/utils/cookies';

axios.defaults.baseURL = BASE_URL;
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Content-Type'] =
  'application/json; application/x-www-form-urlencoded';

export function clientTokenRequest() {
  const token = getTokenAuth();

  const user = store.getState().authenticate.user;
  const client = axios.create({
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return {
    user,
    client,
  };
}

export function clientAnonymousRequest() {
  const client = axios.create({});

  return {
    client,
  };
}
