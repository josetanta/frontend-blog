import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';
import { getUserDetails, updateUser } from 'src/api/user';
import { userUpdateAction } from 'src/app/actions/userActions';

export function useUserAuth() {
  const { isAuth, success, loading, isReady, user, message, error } = useSelector(
    (state) => state.authenticate
  );

  return {
    isAuth,
    isReady,
    user,
    message,
    loading,
    error,
    success,
  };
}

export function useUserDetail(uuidUser) {
  return useQuery(['user-details', uuidUser], () => getUserDetails(uuidUser), {
    select: (detail) => ({ ...detail }),
  });
}

export function useUserAccountUpdate() {
  const dispatch = useDispatch();
  const queryClient = useQueryClient();
  return useMutation(updateUser, {
    onMutate: async (updateUser) => {
      await queryClient.cancelQueries('user-account');
      const previousAccount = queryClient.getQueryData('user-account');

      queryClient.setQueryData('user-account', (prev) => ({
        ...prev,
        updateUser,
      }));

      return previousAccount;
    },
    onError: async (_err, updateUser, context) => {
      queryClient.setQueryData('user-account', { ...context, updateUser });
    },
    onSuccess: async (updateUser) => {
      await queryClient.invalidateQueries('user-account');
      dispatch(userUpdateAction(updateUser));
    },
  });
}
