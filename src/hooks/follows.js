import { useMutation, useQuery, useQueryClient } from 'react-query';
import { follow, getFollowed, getFollowers, unfollow } from 'src/api/follows';

/**
 *
 * @param {string} uuid
 * @returns
 */
export function useFollowersList(uuid) {
  return useQuery(['followers', uuid], () => getFollowers(uuid), {});
}

/**
 *
 * @param {string} uuid
 * @returns
 */
export function useFollowedList(uuid) {
  return useQuery(['followed', uuid], () => getFollowed(uuid), {});
}

export function useFollowMutate({ uuid = '' }) {
  const queryClient = useQueryClient();

  return useMutation(follow, {
    onMutate: async () => {
      await queryClient.cancelQueries(['followers', uuid]);

      const previousFollowers = queryClient.getQueryData(['followers', uuid]);

      queryClient.setQueryData(['followers', uuid], (prev) => ({ ...prev }));

      return previousFollowers;
    },
    onError: (_err, _variables, context) => {
      queryClient.setQueryData(['followers', uuid], context);
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(['followers', uuid]);
    },
  });
}

export function useUnFollowMutate({ uuid = '' }) {
  const queryClient = useQueryClient();

  return useMutation(unfollow, {
    onMutate: async () => {
      await queryClient.cancelQueries(['followers', uuid]);

      const previousFollowers = queryClient.getQueryData(['followers', uuid]);

      queryClient.setQueryData(['followers', uuid], (prev) => ({ ...prev }));

      return previousFollowers;
    },
    onError: (_err, _variables, context) => {
      queryClient.setQueryData(['followers', uuid], context);
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(['followers', uuid]);
    },
  });
}
