export const ENV_DEV = import.meta.env.DEV;
export const API_URL = import.meta.env.VITE_APP_API_URL || '';
export const BASE_URL = `${API_URL}/api`;
