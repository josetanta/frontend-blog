import Follow from './Follow';

export default function FollowList({ follows = [] }) {
  return (
    <div>
      {follows.map((f) => (
        <Follow key={f.id} data={f} />
      ))}
    </div>
  );
}
