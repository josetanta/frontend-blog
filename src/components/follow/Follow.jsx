import { useHistory } from 'react-router-dom';

export default function Follow({ data }) {
  const history = useHistory();

  const handleLinkClick = () => {
    history.push(`/user/${data.uuid}`);
  };

  return (
    <div
      onClick={handleLinkClick}
      className='d-flex gap-4 follow__content justify-content-center align-items-center'
    >
      <img className='account__img__profile' src={data.image} alt={data.name} />
      <span className='fs-5'>{data.email}</span>
    </div>
  );
}
