import { useUserAuth } from 'src/hooks/authentication';
import { useFollowMutate, useUnFollowMutate } from 'src/hooks/follows';
import { Button } from 'react-bootstrap';
import { isFollowing } from 'src/utils/follow';
import React from 'react';

export default function FollowButton({ follows, uuidUser, userId, email }) {
  const auth = useUserAuth();
  const isFollow = isFollowing(auth.user?.id, follows);

  const mutationFollow = useFollowMutate({ uuid: uuidUser });
  const mutationUnFollow = useUnFollowMutate({ uuid: uuidUser });

  const handleFollowClick = () => {
    mutationFollow.mutate(userId);
  };
  const handleUnFollowClick = () => {
    mutationUnFollow.mutate(userId);
  };
  return (
    <React.Fragment>
      {isFollow ? (
        <Button
          className='d-flex flex-column flex-md-row justify-content-between flex-nowrap align-items-center gap-3'
          variant='dark'
          onClick={handleUnFollowClick}
        >
          <span>Dejar de seguir a</span>
          <span className='text-secondary'>{email}</span>
        </Button>
      ) : (
        <Button
          className='d-flex flex-column flex-md-row justify-content-between flex-nowrap align-items-center gap-3'
          variant='success'
          onClick={handleFollowClick}
        >
          <span>Seguir a</span>
          <span span className='text-dark'>
            {email}
          </span>
        </Button>
      )}
    </React.Fragment>
  );
}
