import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useUserAccountUpdate } from 'src/hooks/authentication';
import { AuthPermWrite } from 'src/components/utils/AuthPerm';
import { useUserAuth } from 'src/hooks';

const initState = {
  file: undefined,
  error: undefined,
};

const SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  RESET = 'RESET';

function fileReducer(state = initState, action) {
  switch (action.type) {
    case SUCCESS:
      return { file: action.payload };
    case ERROR:
      return { error: 'La imagen es incorrecta (debe de ser JPG o JPEG)' };

    case RESET:
      return initState;

    default:
      return state;
  }
}
const types = ['image/jpg', 'image/jpeg', 'image/png'];

export default function UserForm({ user, isConfirmed, methodsForm }) {
  const [state, dispatch] = React.useReducer(fileReducer, initState);
  const userAuth = useUserAuth();
  const mutationUpdate = useUserAccountUpdate();
  const watch = methodsForm.watch;

  const handleFileChange = (e) => {
    let selected = e.target.files[0];
    if (selected && types.includes(selected.type)) {
      dispatch({ type: SUCCESS, payload: selected });
    } else {
      dispatch({ type: ERROR });
    }
  };

  const handleFormSubmit = async (data) => {
    const fd = new FormData();
    let emailOriginal = user.email;
    let email = watch('activeEdit') ? emailOriginal : data.email;
    fd.append('name', data.name);
    fd.append('username', data.username);
    fd.append('email', email);
    fd.append('image', data.image[0]);

    await mutationUpdate.mutateAsync(fd);
    mutationUpdate.isSuccess && methodsForm.onReset();
  };

  return (
    <Form onSubmit={methodsForm.handleSubmit(handleFormSubmit)}>
      <div className='mb-3'>
        <strong className='border-bottom'>
          Cuenta{' '}
          {isConfirmed ? (
            <span className='text-success'>Confirmada</span>
          ) : (
            <span className='text-secondary'>No confirmado</span>
          )}
        </strong>
      </div>
      <Form.Group>
        <Form.Label className='text-break' htmlFor='username'>
          Nombre de usuario: <b>{watch('username')}</b>
        </Form.Label>
        <Form.Control
          type='text'
          ref={methodsForm.register}
          className='text-break'
          name='username'
        />
      </Form.Group>
      <Form.Group>
        <Form.Label className='text-break' htmlFor='email'>
          Email: <b>{watch('email')}</b>
        </Form.Label>
        <Form.Control
          ref={methodsForm.register}
          type='email'
          name='email'
          className={[watch('activeEdit') ? 'bg-secondary text-white' : '', 'text-break'].join(' ')}
          disabled={watch('activeEdit')}
        />
        <AuthPermWrite>
          <Form.Check
            type='switch'
            id='custom-switch'
            className='text-info'
            name='activeEdit'
            ref={methodsForm.register}
          />
        </AuthPermWrite>
      </Form.Group>

      <Form.Group>
        <Form.Label className='text-break' htmlFor='name'>
          Nombre: <b>{watch('name')}</b>
        </Form.Label>
        <Form.Control ref={methodsForm.register} className='text-break' type='text' name='name' />
      </Form.Group>

      <Form.Group>
        <Form.Label htmlFor='image'>Imagen de Perfil</Form.Label>
        <Form.File
          ref={methodsForm.register}
          className='file'
          name='image'
          type='file'
          onChange={handleFileChange}
        />
        {state.error && <div className='text-danger'>{state.error}</div>}
      </Form.Group>

      <AuthPermWrite
        msg={
          <React.Fragment>
            Puede reenviar el mensaje de confirmación{' '}
            <Link className='btn btn-outline-secondary' to='/unconfirmed-account'>
              Aquí
            </Link>
          </React.Fragment>
        }
      >
        <Form.Group className='d-flex gap-4 flex-column flex-md-row align-items-center justify-content-between'>
          <Button type='submit' disabled={userAuth.loading} className='btn btn-success  w-100'>
            Actualizar mi perfil
          </Button>
          <Button type='button' onClick={methodsForm.onReset} className='btn btn-dark w-100'>
            Dejar por defecto
          </Button>
        </Form.Group>
      </AuthPermWrite>
    </Form>
  );
}
