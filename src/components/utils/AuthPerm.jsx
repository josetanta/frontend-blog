import React from 'react';
import { useUserAuth } from 'src/hooks/authentication';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export function AuthPerm({ children, user }) {
  const auth = useUserAuth();

  return (
    <React.Fragment>
      {auth.isAuth && auth.user?.uuid === user.uuid ? (
        <React.Fragment>{children}</React.Fragment>
      ) : null}
    </React.Fragment>
  );
}

export function AuthPermFollow({ children, user }) {
  const auth = useUserAuth();

  return (
    <React.Fragment>
      {auth.isAuth && auth.user.confirmed && auth.user?.uuid !== user.uuid ? (
        <React.Fragment>{children}</React.Fragment>
      ) : (
        <React.Fragment />
      )}
    </React.Fragment>
  );
}

export function AuthActionPerm(props) {
  const auth = useUserAuth();

  return auth.isAuth ? <React.Fragment>{props.children}</React.Fragment> : null;
}

export function AuthPermWrite(props) {
  let { message, msg, children } = props;
  const auth = useUserAuth();

  return auth.isAuth ? (
    auth.user.confirmed ? (
      <React.Fragment>{children}</React.Fragment>
    ) : (
      <React.Fragment>
        {message ? (
          <Container>
            <div className='p-2 text-warning bg-transparent'>{message}</div>
            <div className='p-2'>
              Puede enviar un mensaje de confirmacion{'  '}
              <Link className='text-success' to='/unconfirmed-account'>
                Aqui
              </Link>
            </div>
          </Container>
        ) : msg ? (
          <React.Fragment>{msg}</React.Fragment>
        ) : null}
      </React.Fragment>
    )
  ) : null;
}

export function AnonymousPerm(props) {
  const auth = useUserAuth();
  return !auth.isAuth ? <React.Fragment>{props.children}</React.Fragment> : null;
}
