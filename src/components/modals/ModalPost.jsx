import { Button, Modal } from 'react-bootstrap';
import PostForm from 'src/components/forms/PostForm';

function ModalPost({ modalShowPost, setModalShowPost }) {
  return (
    <Modal
      show={modalShowPost}
      onHide={() => setModalShowPost(!modalShowPost)}
      size='lg'
      aria-labelledby='example-modal-sizes-title-lg'
    >
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-center'>Crear una nueva publicación</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <PostForm setModalShowPost={setModalShowPost} modalShowPost={modalShowPost} />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => setModalShowPost(!modalShowPost)} variant='outline-secondary'>
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
export default ModalPost;
