import { useState } from 'react';
import { Button } from 'react-bootstrap';
import ModalEmail from './modals/ModalEmail';

export default function Footer() {
  const [modalShowEmail, setModalShowEmail] = useState(false);
  return (
    <footer className='text-center border-top mt-3'>
      <Button
        variant='link'
        className='text-decoration-none'
        onClick={() => setModalShowEmail(true)}
      >
        jose.tanta.27@unsch.edu.pe
      </Button>
      <ModalEmail modalShowEmail={modalShowEmail} setModalShowEmail={setModalShowEmail} />
    </footer>
  );
}
