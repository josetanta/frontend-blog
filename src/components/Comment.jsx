import { useState } from 'react';

// Components
import { Button } from 'react-bootstrap';
import ImageUser from './ImageUser';
import CommentForm from './forms/CommentForm';
import { AuthPerm } from 'src/components/utils/AuthPerm';
import { useCommentRemoveMutation } from 'src/hooks/comments';
import { formatDatetime } from 'src/utils/moment';

function Comment({ comment }) {
  let user = comment.relationships?.users?.data;
  let post = comment.relationships?.posts?.data;

  const mutationRemove = useCommentRemoveMutation({
    postId: post.id,
    uuidPost: post.uuid,
  });

  const [isEdit, setIsEdit] = useState(false);

  const deleteCommentHandle = () => {
    if (window.confirm('Esta seguro de eliminar su Comentario?')) {
      mutationRemove.mutate(comment.id);
    }
  };

  let actionComment = (
    <AuthPerm user={user}>
      {!isEdit ? (
        <Button variant='light' size='sm' onClick={() => setIsEdit(!isEdit)}>
          <i className='fal fa-pencil' />
        </Button>
      ) : (
        <Button
          variant='dark'
          size='sm'
          onClick={() => setIsEdit(!isEdit)}
          children={<i className='fal fa-times' />}
        />
      )}
      <span className='mr-2' />
      <Button
        variant='outline-danger'
        size='sm'
        onClick={deleteCommentHandle}
        children={<i className='fal fa-trash-alt' />}
      />
    </AuthPerm>
  );
  return (
    <div className='border p-3 shadow'>
      {mutationRemove.isSuccess && (
        <div className='alert alert-warning'>Su comentario ha sido eliminado</div>
      )}
      <div className='d-flex justify-content-between'>
        <ImageUser user={user} classes='mr-3 comment-img-author' showLink />
        <div>{actionComment}</div>
      </div>
      <b className='small text-secondary'>
        <b>Publicado: </b>
        <span className='text-muted'>{formatDatetime(comment.attributes.created)}</span>
      </b>
      {!isEdit ? (
        <div
          className='content-comment px-4 text-break text-justify'
          dangerouslySetInnerHTML={{ __html: comment.attributes.content }}
        />
      ) : (
        <CommentForm
          setIsEdit={setIsEdit}
          isEdit={isEdit}
          content={comment.attributes.content}
          commentId={comment.id}
          postId={post.id}
          uuidPost={post.uuid}
        />
      )}
    </div>
  );
}

export default Comment;
