# my-blog Project create with Flask

- Para este proyecto se hara con react para el frontend

## Estructura del API Rest

## User API

```json
{
  "data": {
    "attributes": {
      "address": "",
      "email": "",
      "followeds": "0",
      "followers": "2",
      "image": "/static/uploads/users/01eabdb5744809565520fa3f.jpg",
      "slug": "",
      "status": true,
      "upload": "01eabdb5744809565520fa3f.jpg",
      "username": ""
    },
    "id": "1",
    "links": {
      "self": "http://<my_site>/api/v1/users/1"
    },
    "relationships": {
      "comments": {
        "data": [
          {
            "id": "2",
            "type": "comments"
          }
        ],
        "links": {
          "related": "http://<my_site>/api/v1/users/1/comments"
        }
      },
      "posts": {
        "data": [
          {
            "id": "1",
            "slug": "postewqe",
            "type": "posts"
          }
        ],
        "links": {
          "related": "http://<my_site>/api/v1/users/1/posts"
        }
      }
    },
    "type": "users"
  }
}
```

# Post API

```json
{
  "data": {
    "attributes": {
      "content": "#ewqe est es un titulo",
      "content_html": "ewqe est es un titulo",
      "created": "Tue, 22 Dec 2020 16:57:08 GMT",
      "image": "/static/uploads/posts/85399a2ea151e90a3c72d92b.jpg",
      "publishied": true,
      "slug": "post-1",
      "title": "Post 1"
    },
    "id": "1",
    "links": {
      "self": "http://<my_site>/api/v1/posts/1"
    },
    "relationships": {
      "comments": {
        "data": [
          {
            "id": "2",
            "type": "comments"
          }
        ],
        "links": {
          "related": "http://<my_site>/api/v1/posts/1/comments"
        }
      },
      "users": {
        "data": {
          "id": "1",
          "slug": "",
          "type": "users"
        },
        "links": {
          "related": "http://<my_site>/api/v1/users/1",
          "self": "http://<my_site>/api/v1/users/1/posts/1"
        }
      }
    },
    "type": "posts"
  }
}
```

## Comment API

```json
{
  "data": {
    "attributes": {
      "content": "Lorem I500 años",
      "created": "Tue, 22 Dec 2020 21:14:32 GMT"
    },
    "id": "3",
    "links": {
      "self": "http://<my_site>/api/v1/comments/3"
    },
    "relationships": {
      "posts": {
        "data": {
          "id": "1",
          "slug": "post-1",
          "type": "posts"
        },
        "links": {
          "related": "http://<my_site>/api/v1/posts/1",
          "self": "http://<my_site>/api/v1/posts/1/comments/3"
        }
      },
      "users": {
        "data": {
          "id": "1",
          "slug": "",
          "type": "users"
        },
        "links": {
          "related": "http://<my_site>/api/v1/users/1",
          "self": "http://<my_site>/api/v1/users/1/comments/3"
        }
      }
    },
    "type": "comments"
  }
}
```
